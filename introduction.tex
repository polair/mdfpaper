Direct observations are the basis of our understanding of the earth's climate system, yet no observations can predict future weather or climate. Since the advent of the computer, models have therefore fill our knowledge gaps in both space and time to providing critical functions in forecasting and climate projection. Model improvement, and thus improved prediction of weather and climate, depends on observations made by in situ research platforms to evaluate a  models physics, internal assumptions, and its resulting predictions. Evaluating model performance is most crucial for cutting edge Numerical Weather Prediction (NWP) forecast models, whose explicit goal is accurate prediction of physical processes for high temporal and spatial resolutions. A goal that is particularly difficult to accomplish in the Arctic, where a lack of in situ observations makes evaluating model performance both challenging and burdensome (CITATION). To facilitate this work, a coordinated effort in organizing, standardizing, and evaluating observations was born out of the Polar Prediction Project (PPP) and a community of international researchers organized by the World Meteorological Organization (WMO) \parencite{WMO2014}. 

The PPP community determined specific tasks work that would allow rapid progress towards more accurate models \parencite{Jung2016}, work motivated by increased human activity in Arctic regions, accelerating changes in Arctic climate, a dearth of local observations, and corresponding model deficits. To carry out these tasks the Year of Polar Prediction Supersite Model Inter-comparison (siteMIP) Project was launched, with the ovearching research goal of evaluating model processes through the following specific tasks:\vspace{3ex}

\columnratio{0.6,0.40}
\begin{paracol}{2}
\setlength{\columnseprule}{0.4pt}
\setlength{\columnsep}{2em}

\begin{leftcolumn}

\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=0ex,parsep=1ex]
\item create a standard data format for in situ observations
\item provide online tools for visualization
\item archive datasets for long-term community use 
\item evaluate "special observing periods", kick off research
\end{enumerate}

\vspace{2ex}

In this publication the authors present software tools that the YOPP siteMIP team has created to accomplish tasks 1) and 2), with the intention of expanding the community of users of these merged datasets and to simplify the standardization tasks for researchers who join in the future to create their own MDFs. For readers interested in a broad overview of the siteMIP project, please reference (CITATION Uttal et al). For more information on 3), the large datasets of both models and observations gathered during two special observing periods (SOPs), SOP1 from February-March 2018 and SOP2 from July-September 2018, please reference (CITATION Morris). To find information on the initial research performed using these data, please reference (CITATION Day). For the remainder of this paper we will describe the implementation details of these data, the software tools we are providing, how to use them, and the future plans for the extension of this work. Because software is ideally constantly improving, this paper provides a basic outline of the purpose and uses. Please refer to the software documentation and examples for specific up-to-date details. 

\end{leftcolumn}
\begin{rightcolumn}

\begin{figure}[H]
  \centering
  \includegraphics[width=1.0\linewidth]{figs/MODF_site_map_round.png}
  \caption{A map of the Arctic supersites where MDFs are currently produced or are in the process of being created, with grid domain overlays for the Environmental Climate Change Canada ``CAPS'' and Météo-France ``AROME'' models.}
  \label{fig:map}
\end{figure}

\end{rightcolumn}
\end{paracol}

\subsection{Merged Data File (MDF) format specification}

The MDF format was created to address common hurdles to be overcome when attempting to use in situ observations for model evaluation. The specification states the metadata and common variables required for the datasets and the common coordinates of these variables. The specification serves two purposes: the first is to simplify utilization of data from observation sites and models to eliminate the work each researcher duplicates when sourcing from disparate teams and the second is to align methodologies such that variables represent the scientific parameters of interest for both sites and models to the furthest extent possible. In this way, the effort expended to create MDFs encapsulates the expertise and knowledge of the researchers who have packaged the data for the consumption by the end users of the resulting standardized product.    

MDFs are provided in the network common data from (netCDF), a machine independent format for packaging scientific data \parencite{Rew1990}. netCDF abstractions allow for grouping, storage, and retreival of multidimensional data such that dissimilar types of data may coexist in a heterogeneous network without relying on formats specific to machines or applications. netCDFs allow a rich and complete syntactic description of any desired metadata for the packaged data and, yes, the MDF format specification requires certain metadata fields for clarity. The purpose of the MDF Toolkit is to make formatting observations from field sites or model outputs to the MDF specification as pain-free as possible.

Climate and forecast (CF) conventions \parencite{Hassell2017} are used as the basis for the metadata descriptions for the MDF data, and the conventions have been expanded by the creation of the Hartten-Khalsa table (HK-table). The HK table specifies variable naming conventions as well as desired metadata both on a variable level and globally. It is a continuously evolving document, available in both human and machine readable formats, that aligns the data requirements of observations and models makers through an iterative discussion process. The variables, metadata, and specifications outlined in the HK-table will improve as additions, alterations, and improvements are suggested by the community at large through the continued development of the project. 

\subsection{Merged Data File (MDF) ``Toolkit''}

By specifying the important variables, their metadata, and the broad formatting for a ``Merged Data File'' to make for efficient model accurate evaluation, a significant amount of progress was made and effort expended. However, it was quickly realized that a range of researchers creating files with a diverse set of tools and dta had a difficult time ensuring that their final files were formatted exactly to the specification. To resolve this, the authors set out to create common tools for the easy creation of properly formatted MDFs and as well as to evaluate whether a particular data file correctly follows the MDF specification. 

The \href{https://doi.org/10.5281/zenodo.7814813}{MDF Toolkit is a free and open source (GPLv3) Python package} that facilitates data processing and formatting, and also provides a programmatic MDF ``checker'' to evaluate a provided file against the current MDF specification. The Toolkit offers many features that will, in an ideal world, save a data processing user time. The Toolkit can track and merge inhomogeneous data at different time cadences, write these data out on the appropriate standard temporal grid, manage vertical variables such as cloud profile information, keep track of the coordinates of different sensors across the site, and more. While the user must manage certain aspects of inputting these types of data, when properly sourced the Toolkit will apply the appropriate post-processing to the provided data, ascribe metadata, and format the resulting file to match the MDF specification by finally writing the resulting data disk.  

The MDF Toolkit is available on PyPI as a standard Python package that can be installed with your tool of choice. Detailed instructions for installation are outlined below in the appendix, in the Toolkit repository, and the online documentation. The Toolkit interface is based on commonly used structures from the ``Pandas'' \parencite{Pandas2023} and ``NumPy'' \parencite{Numpy2020} packages and should therefore plug nicely into any previously programmed data processing routines. 

\subsection{Data Visualization and Archival}

{\bf \vspace{2ex}Johanna, I put this paragraph of yours here. Would you like to more clearly introduce this? \\[2ex]}

Once a standardized MDF has been created for a specific site, datasets are then made publicly available on the YOPP data portal. To further facilitate comprehension and research using these data, the siteMIP team has provided a set of web based visualization/exploration tools. 

The datasets created have been made available through Met Norway’s data portal, under the FAIR principles. This makes the data produced widely available for those who are interested in it. However due to the nature of the available data, programming knowledge is required to interact with the datasets, something that poses a detriment to the usability of the datasets. To remedy this a visualization toolkit was constructed based off a set of common use-cases outlined by those who have used the datasets throughout YOPPsiteMIP for process based NWP evaluation. 
